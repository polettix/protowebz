# proto\_webz - Helm chart

This is an [Helm][] chart to deploy the `protowebz` [Docker][] image in
[Kubernetes][]. It is expected to work fine with Helm version 3.

The contents of this Helm chart are Copyright 2021 by Flavio Poletti.

[Helm]: https://helm.sh/
[Docker]:  https://www.docker.com/
[Kubernetes]: https://kubernetes.io/
