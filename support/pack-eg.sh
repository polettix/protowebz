#!/bin/sh
exec >&2
dst_dir="$1"
version="$2"
image="$3"
tar cC "$(dirname "$0")" eg | tar xC "$dst_dir"
cd "$dst_dir"
mv eg protowebz-examples
sed -i -e "s#__IMAGE__#$image#g" protowebz-examples/run.sh
tar czf examples.tar.gz protowebz-examples
rm -rf protowebz-examples
exit 0
