#!/bin/sh
dst_dir="$1"
version="$2"
docs_dir="$dst_dir/../docs"
tar cC "$docs_dir" text | tar xC "$dst_dir"
cd "$dst_dir"
mv text protowebz-docs-text
tar czf docs-text.tar.gz protowebz-docs-text
rm -rf protowebz-docs-text
exit 0
