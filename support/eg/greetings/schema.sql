PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE people (
   id       INTEGER PRIMARY KEY AUTOINCREMENT,
   name     TEXT,
   greeting TEXT
);
INSERT INTO "people" VALUES(1,'Alice','Ciao');
INSERT INTO "people" VALUES(2,'Bob','Hello');
INSERT INTO "people" VALUES(3,'Carol','Hello');
INSERT INTO "people" VALUES(4,'Denzel','Ciao');
INSERT INTO "people" VALUES(5,'Eveline','Ahoy');
INSERT INTO "people" VALUES(6,'Felix','Hi');
INSERT INTO "people" VALUES(7,'Grace','Ahoy');
INSERT INTO "people" VALUES(8,'Horatio','Ahoy');
DELETE FROM sqlite_sequence;
INSERT INTO "sqlite_sequence" VALUES('example',2);
COMMIT;
