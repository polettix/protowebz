#!/bin/sh
: ${IMAGE:="__IMAGE__"}
example="${1:-"greetings"}"
port="${2:-8080}"

mydir="$(dirname "$(readlink -f "$0")")"
edir="$mydir/$example"
if [ ! -d "$edir" ] ; then
   printf >&2 %s\\n "invalid example directory <$edir>"
   exit 1
fi

docker run --rm \
   -v "$edir:/mnt" \
   -p "$port:8080" \
   -e PROTOWEBZ_SPEC_FILE=/mnt/openapi.yaml \
   -e PROTOWEBZ_DSN=dbi:SQLite:/mnt/db.sqlite \
   -e PROTOWEBZ_DBPASS='' \
   -e PROTOWEBZ_DBUSER='' \
   "$IMAGE" service
