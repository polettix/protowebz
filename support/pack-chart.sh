#!/bin/sh
dst_dir="$1"
version="$2"
tar cC "$(dirname "$0")" chart | tar xC "$dst_dir"
cd "$dst_dir"
mv chart protowebz-chart
sed -i -e "s/__VERSION__/$version/g" protowebz-chart/values.yaml
tar czf chart.tar.gz protowebz-chart
rm -rf protowebz-chart
exit 0
