#!/bin/sh
app_dir="$1"
shift 1

src_dir="$(readlink -f "$(cat DIBS_DIR_SRC)")/support"
dst_dir="$(readlink -f "$app_dir")/archives"

mkdir -p "$dst_dir"
for generator in "$src_dir/"*.sh ; do
   [ -x "$generator" ] || continue
   "$generator" "$dst_dir" "$@"
done

exit 0
