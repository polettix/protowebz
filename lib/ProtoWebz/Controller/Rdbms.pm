package ProtoWebz::Controller::Rdbms;
use Mojo::Base 'ProtoWebz::Controller', -signatures;
use Ouch ':trytiny_var';

sub _get_params ($self) {
   my $spec  = $self->openapi->spec->{'x-protowebz'};
   my $query = $spec->{query};
   my @binds = map { $self->param($_) } ($spec->{binds} // [])->@*;
   return ($query, @binds);
} ## end sub _get_params ($self)

sub single ($self) {
   $self->openapi->valid_input or return;
   $self->respond_ok($self->model->single($self->_get_params));
}

sub list ($self) {
   $self->openapi->valid_input or return;
   $self->respond_ok($self->model->list($self->_get_params));
}

sub indexed ($self) {
   $self->openapi->valid_input or return;
   my ($query, @binds) = $self->_get_params;
   my $key = $self->openapi->spec->{'x-protowebz'}{key};
   $self->respond_ok($self->model->indexed($query, $key, @binds));
} ## end sub indexed ($self)

1;
