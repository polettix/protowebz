package ProtoWebz::Controller::Example;
use Mojo::Base 'ProtoWebz::Controller', -signatures;
use Ouch ':trytiny_var';
use Mojo::Util;

sub _stuff ($what = 'array') {
   state $stuff = [
      {id => 0, nome => 'Galook', messaggio => 'Ahoy!'},
      {id => 1, nome => 'Foo',    messaggio => 'Hello!'},
      {id => 2, nome => 'Bar',    messaggio => 'Salve!'},
   ];
   return $stuff->@* if $what eq 'array';
   return map { $_ => $stuff->[$_] } 0 .. $stuff->$#* if $what eq 'hash';
   return $stuff->[$what]
     if $what =~ m{\A (?: 0 | [1-9]\d* ) \z}mxs && $what <= $stuff->$#*;
   return;
} ## end sub _stuff ($what = 'array')

sub fake ($self) {
   $self->openapi->valid_input or return;
   $self->log->debug(Mojo::Util::dumper($self->openapi->spec));
   $self->respond_ok(_stuff(0));
}

sub single ($self) {
   $self->openapi->valid_input or return;
   my $id = $self->param('id');
   my $object = _stuff($id)
      or ouch 404, "Missing object #$id#", [$id];
   $self->respond_ok($object);
} ## end sub single ($self)

sub list ($self) {
   $self->openapi->valid_input or return;
   $self->respond_ok([_stuff()]);
}

sub indexed ($self) {
   $self->openapi->valid_input or return;
   $self->respond_ok({_stuff('hash')});
}

1;
