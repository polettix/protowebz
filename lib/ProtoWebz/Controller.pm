package ProtoWebz::Controller;
use Mojo::Base 'Mojolicious::Controller', -signatures;
use Mojo::Message::Response;

# standardized way to send a response
sub normalize ($self, @args) {
   my %args = @args == 1 ? $args[0]->%* : @args;
   my %response;
   my $code = $response{status} = $args{code} // $args{status} // 500;
   $response{reason} = $args{reason}
     // Mojo::Message::Response->default_message($code);
   $response{data} = $args{data}
      if exists $args{data} && $code =~ m{\A 2\d\d \z}mxs;
   return \%response;
}

sub respond ($self, %args) {
   my $response = $self->normalize(%args);
   $self->render(status => $response->{status}, openapi => $response);
   return $self;
} ## end sub respond

sub respond_exception ($s, %as) { $s->respond(code => 500, %as) }
sub respond_not_found ($s, %as) { $s->respond(code => 404, %as) }
sub respond_not_ok ($s, $c, %as) { $s->respond(code => $c, %as) }
sub respond_ok ($s, $d, %as) { $s->respond(code => 200, %as, data => $d) }

1;
