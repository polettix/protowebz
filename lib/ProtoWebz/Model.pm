package ProtoWebz::Model;
use Mojo::Base -base, -signatures;
use DBI;
use Ouch ':trytiny_var';

has dbh =>
  sub ($s) { DBI->connect($s->dsn, $s->user, $s->pass, $s->opts) };
has 'dsn';
has opts => sub { return {RaiseError => 1, PrintError => 0} };
has 'pass';
has 'user';

sub single ($self, $query, @binds) {
   $self->dbh->selectrow_hashref($query, {}, @binds)
     or ouch 404, '', [$query, @binds];
}

sub list ($self, $query, @binds) {
   $self->dbh->selectall_arrayref($query, {Slice => {}}, @binds);
}

sub indexed ($self, $query, $key, @binds) {
   $self->dbh->selectall_hashref($query, $key, {}, @binds);
}

1;
