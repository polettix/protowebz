package ProtoWebz;
use YAML::XS;
use Mojo::Base 'Mojolicious', -signatures;
use Mojo::Message::Response;
use Cpanel::JSON::XS;
use Mojo::Util qw< dumper >;
use ProtoWebz::Controller;
use ProtoWebz::Model;
use Ouch ':trytiny_var';
use Try::Catch;
use Scalar::Util 'blessed';

use constant DEFAULT_SECRETS => 'aG9sYS1wZW9wbGU=';
use constant DEFAULT_SPEC    => 'openapi-example.yaml';

# This method will run once at server start
sub startup ($self) {
   $self->moniker(lc __PACKAGE__);

   # Load configuration from config file, override with env variables
   my $config = $self->plugin('NotYAMLConfig');
   for my $key (qw< spec_file dsn dbuser dbpass >) {
      my $env_key = 'PROTOWEBZ_' . uc($key);
      $config->{$key} = $ENV{$env_key} if defined $ENV{$env_key};
   }

   # Configure the application
   $self->controller_class('ProtoWebz::Controller');

   # Load OpenAPI specification, will come handy sometimes
   $config->{spec_file} //= DEFAULT_SPEC;
   $config->{spec} = YAML::XS::LoadFile($config->{spec_file});

   # Rest of the initialization...
   $self->_startup_helpers($config)    # helpers (model)
     ->_startup_hooks($config)         # hooks (around_dispatch)
     ->_startup_plugins($config)       # plugins (OpenAPI)
     ->_startup_secrets($config);      # secrets (whatever comes)
} ## end sub startup ($self)

sub _exception_handler ($self, $c, $e) {
   my %response = (code => 500);
   my $logger = $self->log;
   if (blessed($e) && $e->isa('Ouch')) {
      $logger->debug(join ' ', '(Ouch)', bleep($e), dumper($e->data));
      $response{code}   = $e->code;
      $response{reason} = $e->message;
      $response{data}   = $e->data;
   } ## end if (blessed($e) && $e->...)
   elsif (blessed($e) && $e->can('message')) {
      $logger->debug('(Mojo) ' . $e->message);
   }
   elsif (ref($e)) {
      $logger->debug('(ref) ' . dumper($e));
   }
   else {
      $logger->debug('(plain) ' . $e);
   }
   $response{reason} =
     Mojo::Message::Response->default_message($response{code})
     unless length($response{reason} // '');
   $c->respond(%response);
} ## end sub _exception_handler

sub _startup_helpers ($self, $config) {
   $self->helper(
      model => sub {
         state $m = ProtoWebz::Model->new(
            dsn  => $config->{dsn},
            user => $config->{dbuser},
            pass => $config->{dbpass},
         );
      }
   );
   $self->helper(formatter => sub { state $f = ProtoWebz::Formatter->new }
   );
   return $self;
} ## end sub _startup_helpers

sub _startup_hooks ($self, $config) {
   $self->hook(
      around_dispatch => sub ($next, $c) {
         try { $next->() }
         catch { $self->_exception_handler($c, $_) };
      },
   );
   return $self;
} ## end sub _startup_hooks

sub _startup_plugins ($self, $config) {
   my $spec_file = $config->{spec_file};
   my $spec      = $config->{spec};
   $self->plugin(
      "OpenAPI" => {
         default_response => {
            description => 'ProtoWebz default response',
            name        => 'ProtoWebzDefault',
            schema      => $spec->{components}{schemas}{response_failure},
         },
         renderer => sub ($c, $data) {    # insist on own format...
            state $j =
              Cpanel::JSON::XS->new->utf8->escape_slash->canonical->pretty;
            if (exists $data->{errors}) {
               my $c = $data->{status};
               $data = {
                  status => $c,
                  reason => Mojo::Message::Response->default_message($c),
               };
            } ## end if (exists $data->{errors...})
            return $j->encode($data);
         },
         url => $spec_file,
      }
   );
   return $self;
} ## end sub _startup_plugins

sub __split_and_decode ($s) {
   map { b64_decode($_) } split m{\s+}mxs, $s;
}

sub _startup_secrets ($self, $config) {
   my @secrets =
       defined $ENV{SECRETS}      ? _split_and_decode($ENV{SECRETS})
     : defined $config->{secrets} ? $config->{secrets}->@*
     :                              _split_and_decode(DEFAULT_SECRETS);
   $self->secrets(\@secrets);
   return $self;
} ## end sub _startup_secrets

1;
