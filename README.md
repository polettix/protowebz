# ProtoWebz - prototype web services

This [Mojolicious][] application aims at making it easy to prototype web
applications.

# Authors

Flavio Poletti

# License and Copyright

## Main

Copyright 2021 by Flavio Poletti (flavio@polettix.it).

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

or look for file `LICENSE` in this project's root directory.

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

## Oracle Instant Client

This repository includes a copy of the [Oracle Instant Client][]. These are
unmodified versions of the Basic and SDK ZIP packages.

The Basic package contains the following indication:

> Your use of this copy of Oracle Instant Client software product is subject
> to, and may not exceed the conditions of use for which you are authorized
> under one of the following:
>
> (i) the license or cloud services terms that you accepted when you obtained
> the right to use Oracle Instant Client software; or
>
> (ii) the license terms that you agreed to when you placed your order with
> Oracle for an Oracle product containing Oracle Instant Client software; or
>
> (iii) the Oracle Instant Client software license terms, if any, included with
> the hardware that you acquired from Oracle; or
>
> (iv) the Oracle Technology Network License Agreement (which you acknowledge
> you have read and agree to) available at
> http://www.oracle.com/technetwork/licenses/distribution-license-152002.html;
> or, if (i), (ii), (iii), and or (iv) are not applicable, then,
>
> (v) the Oracle Free Use Terms and Conditions available at BASIC\_LICENSE.
>
> Oracle's obligations with respect to your use of the Oracle Instant Client,
> including, without limitation, obligations to indemnify you, if any, shall
> only be as set forth in the specific license under which you are authorized
> and choose to use Oracle Instant Client.

The `BASIC_LICENSE` file can be found inside the package included in this
repository and also as file `INSTANTCLIENT_BASIC_LICENSE`.

The SDK package contains the following indication:

> Your use of this copy of Oracle Instant Client software product is subject
> to, and may not exceed the conditions of use for which you are authorized
> under one of the following:
> 
> (i) the license or cloud services terms that you accepted when you obtained
> the right to use Oracle Instant Client software; or
> 
> (ii) the license terms that you agreed to when you placed your order with
> Oracle for an Oracle product containing Oracle Instant Client software; or
> 
> (iii) the Oracle Instant Client software license terms, if any, included with
> the hardware that you acquired from Oracle; or
> 
> (iv) the Oracle Technology Network License Agreement (which you acknowledge
> you have read and agree to) available at
> http://www.oracle.com/technetwork/licenses/distribution-license-152002.html;
> or, if (i), (ii), (iii), and or (iv) are not applicable, then,
> 
> (v) the Oracle Free Use Terms and Conditions available at SDK\_LICENSE.
> 
> Oracle's obligations with respect to your use of the Oracle Instant Client,
> including, without limitation, obligations to indemnify you, if any, shall
> only be as set forth in the specific license under which you are authorized
> and choose to use Oracle Instant Client.

The `SDK_LICENSE` file can be found inside the package included in this
repository and also as file `INSTANTCLIENT_SDK_LICENSE`.

## Pandoc

The repository contains a statically compiled copy of [Pandoc][]:

```
$ ./pandoc -v
pandoc 2.11.3.2
Compiled with pandoc-types 1.22, texmath 0.12.1, skylighting 0.10.2,
citeproc 0.3.0.3, ipynb 0.1.0.1
User data directory: ...
Copyright (C) 2006-2020 John MacFarlane. Web:  https://pandoc.org
This is free software; see the source for copying conditions. There is no
warranty, not even for merchantability or fitness for a particular purpose.
```

The copying conditions are available in [Pandoc's repository][pandocrep] as
file [COPYRIGHT][pandoccopy] and also as file `PANDOC_COPYRIGHT`.

[Mojolicious]: https://metacpan.org/pod/Mojolicious
[Oracle Instant Client]: https://www.oracle.com/database/technologies/instant-client.html
[Pandoc]: https://pandoc.org/
[pandocrep]: https://github.com/jgm/pandoc
[pandoccopy]: https://github.com/jgm/pandoc/blob/2.11.3.2/COPYRIGHT
