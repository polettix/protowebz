#!/usr/bin/env perl
use v5.24;
use warnings;
use Mojolicious::Lite -signatures;

get "/item/:id" => sub ($c) {
   $c->openapi->valid_input or return;
   my $id = $c->param('id');
   if ($id == 404) {
      $c->render(
         status  => 404,
         openapi => {status => 404, reason => 'Not Found'}
      );
   } ## end if ($c->param('id') > ...)
   elsif ($id == 500) {
      $c->render(
         status  => 500,
         openapi => {status => 500, reason => 'Server Error'}
      );
   } ## end if ($c->param('id') > ...)
   else {
      $c->render(
         status  => 200,
         openapi => {status => 200, reason => 'OK!'}
      );
   } ## end else [ if ($c->param('id') > ...)]
  },
  'item';

plugin OpenAPI => {url => "data:///spec.yaml"};
app->start;

__DATA__
@@ spec.yaml
openapi: 3.0.0
info:
  title: Example Web API
  version: 0.0.1
  description: |
    PoC definition
components:
  schemas:
    base:
      type: object
      required: [status, reason]
      properties:
        status:
          type: integer
        reason:
          type: string
    not_found:
      type: object
      allOf:
        - $ref: '#/components/schemas/base'
    exception:
      type: object
      allOf:
        - $ref: '#/components/schemas/base'
  responses:
    '404':
      description: Not Found
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/not_found'
paths:
  /item/{id}:
    get:
      summary: get a single item
      description: get a single item from the database
      x-mojo-name: item
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: integer
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/base'
        '404':
          $ref: '#/components/responses/404'
        '500':
          description: Server Error...
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/exception'
